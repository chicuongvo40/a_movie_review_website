﻿using a_movie_review_website.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website
{
    public class AppDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseInMemoryDatabase("testDatabase");
            var connect = "Data Source=DESKTOP-ETC4R5E\\CHICUONG;Initial Catalog=DataMovie;User ID=sa;Password=123456;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            optionsBuilder.UseSqlServer(connect);

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(
                 new Movie
                 {
                     Id = 1,
                     Title = "The Shawshank Redemption",
                     ReleaseDate = new DateTime(1994, 10, 14),
                     Director = "Frank Darabont",
                     Description = "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
                     ImageUrl = "https://www.imdb.com/title/tt0111161/mediaviewer/rm1300568832/",
                     Rating = 9.3m
                 },
                 new Movie
                 {
                     Id = 2,
                     Title = "The Godfather",
                     ReleaseDate = new DateTime(1972, 3, 24),
                     Director = "Francis Ford Coppola",
                     Description = "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
                     ImageUrl = "https://www.imdb.com/title/tt0068646/mediaviewer/rm593544192/",
                     Rating = 9.2m
                 },
                 new Movie
                 {
                     Id = 3,
                     Title = "The Dark Knight",
                     ReleaseDate = new DateTime(2008, 7, 18),
                     Director = "Christopher Nolan",
                     Description = "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.",
                     ImageUrl = "https://www.imdb.com/title/tt0468569/mediaviewer/rm1190567169/",
                     Rating = 9.0m
                 }
                 );



            // Seed the database with sample reviews
            modelBuilder.Entity<Review>().HasData(
                new Review
                {
                    Id = 1,
                    Title = "Amazing movie",
                    Content = "This is one of the best movies I've ever seen. The acting is incredible and the story is so moving.",
                    Rating = 10,
                    MovieId = 1
                },
                new Review
                {
                    Id = 2,
                    Title = "Classic",
                    Content = "The Godfather is a classic movie that everyone should watch. The performances are fantastic and the story is gripping.",
                    Rating = 9,
                    MovieId = 2
                },
                new Review
                {
                    Id = 3,
                    Title = "Great superhero movie",
                    Content = "The Dark Knight is a great superhero movie that stands out from the rest. Heath Ledger's performance as the Joker is amazing.",
                    Rating = 8,
                    MovieId = 3
                }
            );


        }
    }
}



