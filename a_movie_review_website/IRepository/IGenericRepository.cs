﻿using Movie_Console_App_Advance.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.IRepository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void SoftRemove(T entity);
        Task<List<T>> GetAllAsync(int pageIndex = 0, int pageSize = 10);
        Task<T?> GetByIdAsync(int id);
    }

}
