﻿using a_movie_review_website.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.IRepository
{
    public interface IReviewRepository  : IGenericRepository<Review>
    {
    }
}
