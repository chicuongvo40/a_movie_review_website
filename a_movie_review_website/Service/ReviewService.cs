﻿
using a_movie_review_website.Entity;
using a_movie_review_website.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.Service
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddReview()
        {
            Console.Write("Enter Movie Id: ");
            if (!int.TryParse(Console.ReadLine(), out int movieId))
            {
                Console.WriteLine("Invalid Id");
                return;
            }

            Console.Write("Enter Title: ");
            string title = Console.ReadLine();

            Console.Write("Enter Content: ");
            string content = Console.ReadLine();

            Console.Write("Enter Rating: ");
            if (!decimal.TryParse(Console.ReadLine(), out var rating))
            {
                Console.WriteLine("Invalid rating");
                return;
            }

            var review = new Review
            {
                MovieId = movieId,
                Title = title,
                Content = content,
                Rating = (int)rating
            };

            await _unitOfWork.ReviewRepository.AddAsync(review);
            await _unitOfWork.SaveChangeAsync();

            Console.WriteLine("Review added successfully");
        }

        public async Task DeleteReview(int id)
        {
            var review = await _unitOfWork.ReviewRepository.GetByIdAsync(id);

            if (review == null)
            {
                Console.WriteLine($"Review with ID {id} not found");
                return;
            }

            _unitOfWork.ReviewRepository.SoftRemove(review);
            await _unitOfWork.SaveChangeAsync();

            Console.WriteLine("Review deleted successfully");
        }

        public async Task UpdateReview(int id)
        {
            var review = await _unitOfWork.ReviewRepository.GetByIdAsync(id);

            if (review == null)
            {
                Console.WriteLine($"Review with ID {id} not found");
                return;
            }

            Console.Write("Enter Title: ");
            string title = Console.ReadLine();

            Console.Write("Enter Content: ");
            string content = Console.ReadLine();

            Console.Write("Enter Rating: ");
            if (!decimal.TryParse(Console.ReadLine(), out var rating))
            {
                Console.WriteLine("Invalid rating");
                return;
            }

            review.Title = string.IsNullOrWhiteSpace(title) ? review.Title : title;
            review.Content = string.IsNullOrWhiteSpace(content) ? review.Content : content;
            review.Rating = (int)rating;

            _unitOfWork.ReviewRepository.Update(review);
            await _unitOfWork.SaveChangeAsync();

            Console.WriteLine("Review updated successfully");
        }
    }

}

