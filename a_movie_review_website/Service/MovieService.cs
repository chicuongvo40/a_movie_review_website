﻿using a_movie_review_website.IRepository;
using a_movie_review_website.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.Service
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddMovie()
        {
            Console.Write("Enter Title: ");
            var title = Console.ReadLine();
            Console.Write("Enter Release Date (yyyy-MM-dd): ");
            if (!DateTime.TryParse(Console.ReadLine(), out DateTime date))
            {
                Console.WriteLine("Invalid date");
                return;
            }
            Console.Write("Enter Director: ");
            var director = Console.ReadLine();
            Console.Write("Enter Description: ");
            var description = Console.ReadLine();
            Console.Write("Enter Image Url: ");
            var imageUrl = Console.ReadLine();
            Console.Write("Enter Rating (0-10): ");
            if (!decimal.TryParse(Console.ReadLine(), out decimal rating) || rating < 0 || rating > 10)
            {
                Console.WriteLine("Invalid rating");
                return;
            }
            // Tạo đối tượng bộ phim mới với thông tin từ người dùng
            var movie = new Movie
            {
                Title = title,
                ReleaseDate = date,
                Director = director,
                Description = description,
                ImageUrl = imageUrl,
                Rating = rating
            };
            // Thêm đối tượng bộ phim mới vào cơ sở dữ liệu
            await _unitOfWork.MovieRepository.AddAsync(movie);
            await _unitOfWork.SaveChangeAsync();
            Console.WriteLine("Add new movie successfully");
        }

        public async Task DeleteMovie(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is not null)
            {
                _unitOfWork.MovieRepository.SoftRemove(movieObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Delete movie successfully");
            }
            else
            {
                Console.WriteLine("Invalid movieId.");
            }
        }

        public async Task UpdateMovie(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is not null)
            {
                Console.Write("Enter Title: ");
                var title = Console.ReadLine();
                Console.Write("Enter Release Date (yyyy-MM-dd): ");
                if (!DateTime.TryParse(Console.ReadLine(), out DateTime date))
                {
                    Console.WriteLine("Invalid date");
                    return;
                }
                Console.Write("Enter Director: ");
                var director = Console.ReadLine();
                Console.Write("Enter Description: ");
                var description = Console.ReadLine();
                Console.Write("Enter Image Url: ");
                var imageUrl = Console.ReadLine();
                Console.Write("Enter Rating (0-10): ");
                if (!decimal.TryParse(Console.ReadLine(), out decimal rating) || rating < 0 || rating > 10)
                {
                    Console.WriteLine("Invalid rating");
                    return;
                }
                movieObj.Title = string.IsNullOrEmpty(title) ? movieObj.Title : title;
                movieObj.ReleaseDate = date;
                movieObj.Director = string.IsNullOrEmpty(director) ? movieObj.Director : director;
                movieObj.Description = string.IsNullOrEmpty(description) ? movieObj.Description : description;
                movieObj.ImageUrl = string.IsNullOrEmpty(imageUrl) ? movieObj.ImageUrl : imageUrl;
                movieObj.Rating = rating;

                _unitOfWork.MovieRepository.Update(movieObj);
                await _unitOfWork.SaveChangeAsync();
                Console.WriteLine("Update movie successfully");
            }
            else
            {
                Console.WriteLine("Invalid movieId.");
            }
        }

        public async Task<List<Movie>> ViewAllMovie()
        {
            var movies = await _unitOfWork.MovieRepository.GetAllAsync();
            if (movies.Any())
            {
                Console.WriteLine("ID\tTitle\t\t\tRelease Date\tDirector\t\tRating");
                foreach (var movie in movies)
                {
                    Console.WriteLine($"{movie.Id}\t{movie.Title}\t\t{movie.ReleaseDate:d}\t\t{movie.Director}\t\t{movie.Rating}");
                }
            }
            else
            {
                Console.WriteLine("No movies found.");
            }
            return movies;
        }
        public async Task<Movie> ViewMovieReview(int id)
        {
            var movieObj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (movieObj is null)
            {
                Console.WriteLine("Invalid movieId.");
                return null;
            }

            Console.WriteLine($"{movieObj.Title} ({movieObj.ReleaseDate:d}) directed by {movieObj.Director}");

            if (movieObj.Reviews.Any())
            {
                Console.WriteLine("ID\tTitle\t\t\tContent\t\tRating");
                foreach (var review in movieObj.Reviews)
                {
                    Console.WriteLine($"{review.Id}\t{review.Title}\t{review.Content}\t{review.Rating}");
                }
            }
            else
            {
                Console.WriteLine("No reviews found.");
            }

            return movieObj;
        }
    }
}

