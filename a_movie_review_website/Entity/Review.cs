﻿using Movie_Console_App_Advance.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.Entity
{
    public class Review : BaseEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
