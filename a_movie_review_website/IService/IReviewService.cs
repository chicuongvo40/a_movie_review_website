﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.IService
{
    public interface IReviewService
    {
        Task AddReview();
        Task UpdateReview(int id);
        Task DeleteReview(int id);
    }

}
