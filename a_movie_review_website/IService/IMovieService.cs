﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.IService
{
    public interface IMovieService
    {
        Task AddMovie();
        Task UpdateMovie(int id);
        Task DeleteMovie(int id);
        Task<List<Movie>> ViewAllMovie();
        
        Task<Movie> ViewMovieReview(int id);
    }

}
