﻿using a_movie_review_website.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDbContext context) : base(context) { }

        public async Task<Movie> GetMovieReview(int id) => await _dbSet.Include(x => x.Reviews).FirstOrDefaultAsync(x => x.Id == id);

    }
}
