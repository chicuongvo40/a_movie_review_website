﻿using a_movie_review_website.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website
{
    public interface IUnitOfWork : IDisposable
    {
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public Task<int> SaveChangeAsync();
    }

}
