﻿using a_movie_review_website.IRepository;
using a_movie_review_website.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace a_movie_review_website
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        private readonly IMovieRepository _movieRepository;
        private readonly IReviewRepository _reviewRepository;

        public UnitOfWork(AppDbContext context)
        {
            _dbContext = context;
            _movieRepository = new MovieRepository(_dbContext);
            _reviewRepository = new ReviewRepository(_dbContext);
        }

        public IMovieRepository MovieRepository => _movieRepository;
        public IReviewRepository ReviewRepository => _reviewRepository;

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public async Task<int> SaveChangeAsync() => await _dbContext.SaveChangesAsync();
    }

}
