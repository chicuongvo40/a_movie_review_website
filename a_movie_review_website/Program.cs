﻿using a_movie_review_website.Entity;
using a_movie_review_website.Service;
using Microsoft.EntityFrameworkCore;

namespace a_movie_review_website
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            var unitOfWork = new UnitOfWork(new AppDbContext());
            var movieService = new MovieService(unitOfWork);
            var reviewService = new ReviewService(unitOfWork);

            while (true)
            {
                Console.WriteLine("Please select an option:");
                Console.WriteLine("1. View all movies");
                Console.WriteLine("2. View a specific movie and its reviews");
                Console.WriteLine("3. Add a new movie");
                Console.WriteLine("4. Edit an existing movie");
                Console.WriteLine("5. Delete a movie");
                Console.WriteLine("6. Add a new review for a movie");
                Console.WriteLine("7. Edit an existing review for a movie");
                Console.WriteLine("8. Delete a review for a movie");
                Console.WriteLine("0. Exit");

                int option;
                if (!int.TryParse(Console.ReadLine(), out option))
                {
                    Console.WriteLine("Invalid option.");
                    continue;
                }

                switch (option)
                {
                    case 1:
                        Console.WriteLine("All movies:");
                        var movies = await movieService.ViewAllMovie();
                        foreach (var movie in movies)
                        {
                            Console.WriteLine($"- {movie.Id} | {movie.Title} {movie.ReleaseDate}");
                        }
                        break;

                    case 2:
                        Console.Write("Input MovieId: ");
                        if (!int.TryParse(Console.ReadLine(), out int movieId))
                        {
                            Console.WriteLine("Invalid Id");
                            continue;
                        }

                        var movieWithReviews = await movieService.ViewMovieReview(movieId);
                        if (movieWithReviews == null)
                        {
                            Console.WriteLine($"Movie with ID {movieId} not found");
                            continue;
                        }

                        Console.WriteLine($"Movie: {movieWithReviews.Title} {movieWithReviews.ReleaseDate} {movieWithReviews.Director}");
                        Console.WriteLine("Reviews:");
                        foreach (var review in movieWithReviews.Reviews)
                        {
                            Console.WriteLine($"- {review.Title} {review.Content} {review.Rating}");
                        }
                        break;

                    case 3:
                        await movieService.AddMovie();
                        break;

                    case 4:
                        Console.Write("Enter Movie Id: ");
                        if (!int.TryParse(Console.ReadLine(), out int editMovieId))
                        {
                            Console.WriteLine("Invalid Id");
                            continue;
                        }

                        await movieService.UpdateMovie(editMovieId);
                        break;

                    case 5:
                        Console.Write("Enter Movie Id to Delete: ");
                        if (!int.TryParse(Console.ReadLine(), out int deleteMovieId))
                        {
                            Console.WriteLine("Invalid Id");
                            continue;
                        }

                        await movieService.DeleteMovie(deleteMovieId);
                        break;

                    case 6:
                        await reviewService.AddReview();
                        break;

                    case 7:
                        Console.Write("Enter Review Id: ");
                        if (!int.TryParse(Console.ReadLine(), out int editReviewId))
                        {
                            Console.WriteLine("Invalid Id");
                            continue;
                        }

                        await reviewService.UpdateReview(editReviewId);
                        break;

                    case 8:
                        Console.Write("Enter Review Id: ");
                        if (!int.TryParse(Console.ReadLine(), out int deleteReviewId))
                        {
                            Console.WriteLine("Invalid Id");
                            continue;
                        }

                        await reviewService.DeleteReview(deleteReviewId);
                        break;

                    case 0:
                        Console.WriteLine("Exit");
                        return;
                    default:
                        Console.WriteLine("Invalid option.");
                        break;
                }
            }
        }
    }
}

